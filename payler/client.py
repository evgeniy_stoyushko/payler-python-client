import decimal
import json
import logging
import requests
from typing import Optional
from .exceptions import PaylerError
from .enums import *
from .models import *
from .utils import *


logger = logging.getLogger(__name__)


class Payler:
    """Payler client."""

    def __init__(self, host: str, key: str, password: str,
                 debug: Optional[bool]=False) -> None:
        self.host = host
        self.key = key
        self.password = password
        self.debug = debug

    def _send_request(self, endpoint, data=None, params=None, headers=None):
        url = self.host + endpoint
        if self.debug:
            logger.info(f'PAYLER-REQUEST: url {url}, data {data}, '
                        f'headers {headers}')
        request = requests.post(self.host + endpoint, data=data,
                                headers=headers)
        response = request.json(parse_float=decimal.Decimal)
        if self.debug:
            logger.info(f'PAYLER-RESPONSE: {response}')
        if 'error' in response:
            raise PaylerError(response['error'])
        return response

    # Saving card data
    def customer_register(self, api: API, name: Optional[str] = None,
                          phone: Optional[str] = None,
                          email: Optional[str] = None,
                          full_name: Optional[str] = None,
                          address: Optional[str] = None,
                          document_type: Optional[str] = None,
                          document_seria: Optional[str] = None,
                          document_number: Optional[str] = None) -> Customer:
        assert api in ALL_API, 'API does not support this command'
        endpoint = f'/{api.value}/CustomerRegister'
        optional_attrs = {
            'customer_name': name,
            'customer_phone': phone,
            'customer_email': email,
            'customer_fullName': full_name,
            'customer_address': address,
            'customer_documentType': document_type,
            'customer_documentSeria': document_seria,
            'customer_documentNumber': document_number,
        }
        data = {'key': self.key}
        data.update(clean_attrs(optional_attrs))
        response = self._send_request(endpoint=endpoint, data=data)
        return Customer.from_dict(response)

    def customer_update(self):
        # todo: make method
        pass

    def customer_delete(self, api: API, customer_id: str):
        assert api in ALL_API, 'API does not support this command'
        endpoint = f'/{api.value}/CustomerDelete'
        data = {
            'key': self.key,
            'customer_id': customer_id
        }
        response = self._send_request(endpoint=endpoint, data=data)
        # todo: update response
        return response

    def customer_get_status(self):
        # todo: make method
        pass

    def start_save_card_session(
            self,
            api: API,
            customer_id: str,
            template: Optional[str] = None,
            lang: Optional[Lang] = None,
            pay_page_params: Optional[dict] = None,
            **kwargs,
    ) -> Session:
        assert api in GATE_API, 'API does not support this command'
        endpoint = f'/{api.value}/StartSaveCardSession'
        optional_attrs = {
            'template': template,
            'lang': lang if lang is None else lang.value
        }
        if pay_page_params:
            optional_attrs.update(
                {'pay_page_param_' + key: value for key, value in pay_page_params.items()}
            )
        data = {
            'key': self.key,
            'customer_id': customer_id
        }
        data.update(clean_attrs(optional_attrs))
        response = self._send_request(endpoint=endpoint, data=data)
        return Session.from_dict(response)

    def save_url(self, api: API, session_id: str) -> str:
        assert api in GATE_API, 'API does not support this command'
        return f'{self.host}/{api.value}/Save?session_id={session_id}'

    def get_status_save_card(self, api: API, session_id: Optional[str] = None,
                             card_id: Optional[str] = None) ->StatusSaveCard:
        assert api in ALL_API, 'API does not support this command'
        assert session_id or card_id, 'one of these arguments session_id or ' \
                                      'card_id must be filled'
        endpoint = f'/{api.value}/GetStatusSaveCard'
        optional_attrs = {
            'session_id': session_id,
            'card_id': card_id
        }
        data = {'key': self.key}
        data.update(clean_attrs(optional_attrs))
        response = self._send_request(endpoint=endpoint, data=data)
        return StatusSaveCard.from_dict(response)

    def get_card_list(self):
        # todo: make method
        pass

    def remove_card(self, api: API, card_id: str):
        # todo: make answer
        assert api in ALL_API, 'API does not support this command'
        endpoint = f'/{api.value}/RemoveCard'
        data = {'key': self.key, 'card_id': card_id}
        return self._send_request(endpoint=endpoint, data=data)

    def save_card(self):
        # todo: make method
        pass

    # Payment
    def start_session(
            self, payment_type: PaymentType, order_id: str,
            amount: int, email: str, api: Optional[API] = API.GAPI,
            session_type: Optional[SessionPageType] = None,
            customer_id: Optional[str] = None,
            currency: Optional[Currency] = None,
            product: Optional[str] = None,
            template: Optional[str] = None,
            lang: Optional[Lang] = None,
            userdata: Optional[str] = None,
            recurrent: Optional[RecurrentType] = None,
            airline_addendum: Optional[str] = None,
            payment_methods: Optional[str] = None,
            lifetime: Optional[int] = None,
            return_url_success: Optional[str] = None,
            return_url_decline: Optional[str] = None,
            pay_page_params: Optional[dict] = None,
            **kwargs,
    ) -> PaymentSession:
        assert api == API.GAPI, 'API does not support this command'
        assert not (session_type == SessionPageType.SELECT_CARD_PAGE and
                    customer_id is None), \
            'customer_id must be filled for select_card_page session type'
        endpoint = f'/{api.value}/StartSession'
        data = {
            'key': self.key,
            'type': payment_type.value,
            'order_id': order_id,
            'amount': amount,
            'email': email
        }
        optional_attrs = {
            'session_type': session_type if session_type is None else \
                session_type.value,
            'customer_id': customer_id,
            'currency': currency if currency is None else currency.value,
            'product': product,
            'template': template,
            'lang': lang if lang is None else lang.value,
            'userdata': userdata,
            'recurrent': recurrent if recurrent is None else recurrent.value,
            'lifetime': lifetime,
            'return_url_success': return_url_success,
            'return_url_decline': return_url_decline,
            'payment_methods': payment_methods,
            'airline_addendum': airline_addendum,
        }
        if pay_page_params:
            optional_attrs.update(
                {'pay_page_param_' + key: value for key, value in pay_page_params.items()}
            )
        for key, value in kwargs.items():
            if key.startswith('pay_page_param_') or \
                    key.startswith('antifraut_'):
                optional_attrs.update({key: value})
        data.update(clean_attrs(optional_attrs))
        response = self._send_request(endpoint=endpoint, data=data)
        return PaymentSession.from_dict(response)

    def pay_gate(self, session_id: str, api: Optional[API] = API.GAPI):
        assert api == API.GAPI, 'API does not support this command'
        return f'{self.host}/{api.value}/Pay?session_id={session_id}'

    def find_session(self, order_id: str):
        # todo: make method
        pass

    def pay_merchant(self):
        # todo: make method
        pass

    def block(self):
        # todo: make method
        pass

    def charge(self, api: API, order_id: str, amount: int) -> Charge:
        assert api in (API.GAPI, API.MAPI), 'API does not support this command'
        endpoint = f'/{api.value}/Charge'
        data = {
            'key': self.key,
            'password': self.password,
            'order_id': order_id,
            'amount': amount
        }
        response = self._send_request(endpoint=endpoint, data=data)
        return Charge.from_dict(response)

    def retrieve(self):
        # todo: make method
        pass

    def refund(self):
        # todo: make method
        pass

    def repeat_pay(self, order_id: str, amount: int,
                   api: Optional[API] = API.GAPI,
                   recurrent_template_id: Optional[str] = None,
                   card_id: Optional[str] = None,
                   currency: Optional[Currency] = None,
                   userdata: Optional[str] = None, **kwargs) -> RepeatPay:
        assert api == API.GAPI, 'API does not support this command'
        assert recurrent_template_id or card_id, \
            'one of these arguments recurrent_template_id or card_id ' \
            'must be filled'
        endpoint = f'/{api.value}/v1/RepeatPay'
        data = {
            'key': self.key,
            'order_id': order_id,
            'amount': amount,
        }
        optional_attrs = {
            'recurrent_template_id': recurrent_template_id,
            'card_id':  card_id,
            'currency': currency if currency is None else currency.value,
            'userdata': userdata
        }
        for key, value in kwargs.items():
            if key.startswith('pay_page_param_'):
                optional_attrs.update({key: value})
        data.update(clean_attrs(optional_attrs))
        response = self._send_request(endpoint=endpoint, data=data)
        return RepeatPay.from_dict(response)

    def get_template(self):
        # todo: make method
        pass

    def activate_template(self):
        # todo: make method
        pass

    def get_status(self, api: API, order_id: str) -> OperationStatus:
        assert api in ALL_API, 'API does not support this command'
        endpoint = f'/{api.value}/GetStatus'
        data = {'key': self.key, 'order_id': order_id}
        response = self._send_request(endpoint=endpoint, data=data)
        return OperationStatus.from_dict(response)

    def get_advanced_status(self, api: API, order_id: str) -> OperationAdvancedStatus:
        assert api in ALL_API, 'API does not support this command'
        endpoint = f'/{api.value}/GetAdvancedStatus'
        data = {'key': self.key, 'order_id': order_id}
        response = self._send_request(endpoint=endpoint, data=data)
        return OperationAdvancedStatus.from_dict(response)

    def apple_pay(self):
        # todo: make method
        pass

    def google_pay(self):
        # todo: make method
        pass

    def send_3ds(self):
        # todo: make method
        pass

    # Transfer
    def start_credit_session(self):
        # todo: make method
        pass

    def credit_gate(self):
        # todo: make method
        pass

    def credit_merchant(self):
        # todo: make method
        pass

    def repeat_credit(
            self, order_id: str, amount: int, card_id: str,
            api: Optional[API] = API.CGAPI,
            customer_id: Optional[str] = None,
            currency: Optional[Currency] = Currency.RUB,
            lang: Optional[Lang] = None,
            userdata: Optional[str] = None,
            pay_page_params: Optional[dict] = None,
            **kwargs,
    ):
        assert api == API.CGAPI, 'API does not support this command'
        endpoint = f'/{api.value}/RepeatCredit'
        data = {
            'key': self.key,
            'password': self.password,
            'order_id': order_id,
            'amount': amount,
            'card_id': card_id,
        }
        optional_attrs = {
            'customer_id': customer_id,
            'lang': lang if lang is None else lang.value,
            'userdata': userdata,
            'currency': currency.value
        }
        if pay_page_params:
            optional_attrs.update(
                {'pay_page_param_' + key: value for key, value in pay_page_params.items()}
            )
        for key, value in kwargs.items():
            if key.startswith('pay_page_param_'):
                optional_attrs.update({key: value})
        data.update(clean_attrs(optional_attrs))
        response = self._send_request(endpoint=endpoint, data=data)
        return RepeatCredit.from_dict(response)

    def get_balance(self):
        # todo: make method
        pass

    # Receipt

    def receipt(self, receipt: Receipt):
        endpoint = '/kkt/v2/Receipt'
        data = receipt.as_dict()
        data.update({'key': self.key})
        response = self._send_request(endpoint=endpoint, data=json.dumps(data))
        return ReceiptResponse.from_dict(response)

    def get_receipt_status(self):
        # todo: make method
        pass
