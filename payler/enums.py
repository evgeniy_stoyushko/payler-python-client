import enum

__all__ = ['API', 'GATE_API', 'MERCHANT_API', 'ALL_API', 'Lang', 'CardStatus',
           'SessionPageType', 'Currency', 'RecurrentType',
           'ReceiptType', 'SNOType', 'PaymentMethodType', 'AgentType',
           'PaymentObjectType', 'PaymentType', 'VatType', 'TransactionStatus',
           'ReceiptPaymentType']


class API(enum.Enum):

    GAPI = 'gapi'  # GATE - pay
    CGAPI = 'cgapi'  # GATE - credit
    MAPI = 'mapi'  # Merchant - pay
    CMAPI = 'cmapi'  # Merchant - credit


GATE_API = (API.GAPI, API.CGAPI)
MERCHANT_API = (API.MAPI, API.CMAPI)
ALL_API = GATE_API + MERCHANT_API


ERROR_MESSAGES = {

}


class Currency(enum.Enum):
    
    RUB = 'RUB'
    USD = 'USD'
    EUR = 'EUR'
    GBP = 'GBP'
    PLN = 'PLN'
    TJS = 'TJS'


class Lang(enum.Enum):

    RU = 'ru'
    EN = 'en'


class CardStatus(enum.Enum):

    SAVED = 'Saved'
    INVALID = 'Invalid'


class PaymentType(enum.Enum):

    ONE_STEP = 'OneStep'
    TWO_STEP = 'TwoStep'


class SessionPageType(enum.Enum):

    DEFAULT_PAGE = 0
    SELECT_CARD_PAGE = 1


class RecurrentType(enum.Enum):

    REQUIRED = 1
    NOT_REQUIRED = 0


class ReceiptType(enum.Enum):

    SELL = 'sell'  # Приход
    SELL_REFUND = 'sell_refund'  # Возврат прихода
    SELL_CORRECTION = 'sell_correction'  # Коррекция прихода
    BUY = 'buy'  # Расход
    BUY_REFUND = 'buy_refund'  # Возврат расхода
    BUY_CORRECTION = 'buy_correction'  # Коррекция расхода


class SNOType(enum.Enum):

    OSN = 'osn'  # общая СН
    USN_INCOME = 'usn_income'  # УСН (доходы)
    USN_INCOME_OUTCOME = 'usn_income_outcome'  # УСН (доходы минус расходы)
    ENVD = 'envd'  # единый налог на вмененный доход
    ESN = 'esn'  # единый сельскохозяйственный налог
    PATENT = 'patent'  # патентная СН


class AgentType(enum.Enum):

    BANK_PAYING_AGENT = 'bank_paying_agent'  # банковский платежный агент
    BANK_PAYING_SUBAGENT = 'bank_paying_subagent'  # банковский платежный субагент
    PAYING_AGENT = 'paying_agent'  # платежный агент
    PAYING_SUBAGENT = 'paying_subagent'  # платежный субагент
    ATTORNEY = 'attorney'  # поверенный
    COMMISSION_AGENT = 'commission_agent'  # комиссионер
    ANOTHER = 'another'  # другой тип агента


class PaymentMethodType(enum.Enum):

    FULL_PREPAYMENT = 'full_prepayment'  # предоплата 100%
    PREPAYMENT = 'prepayment'  # предоплата
    ADVANCE = 'advance'  # аванс
    FULL_PAYMENT = 'full_payment'  # полный расчет
    PARTIAL_PAYMENT = 'partial_payment'  # частичный расчет и кредит
    CREDIT = 'credit'  # передача в кредит
    CREDIT_PAYMENT = 'credit_payment'  # оплата кредита


class PaymentObjectType(enum.Enum):

    COMMODITY = 'commodity'  # товар
    EXCISE = 'excise'  # подакцизный товар
    JOB = 'job'  # работа
    SERVICE = 'service'  # услуга
    GAMBLING_BET = 'gabling_bet'  # ставка азартной игры
    GABLING_PRIZE = 'gabling_prize'  # выигрыш азартной игры
    LOTTERY = 'lottery'  # лотерейный билет
    LOTTERY_PRIZE = 'lottery_prize'  # выигрыш лотереи
    INTELLECTUAL_ACTIVITY = 'intellectual_activity'  # результаты интеллектуальной деятельности
    PAYMENT = 'payment'  # платеж
    AGENT_COMMISSION = 'agent_commission'  # агентское вознаграждение
    PROPERTY_RIGHT = 'property_right'  # имущественное право
    NON_OPERATING_GAIN = 'non-operating_gain'  # внереализационный доход
    INSURANCE_PREMIUM = 'insurance_premium'  # страховые взносы
    SALES_TAX = 'sales_tax'  # торговый сбор
    RESORT_FEE = 'resort_fee'  # курортный сбор
    COMPOSITE = 'composite'  # составной предмет расчета
    ANOTHER = 'another'  # иной предмет расчета


class ReceiptPaymentType(enum.Enum):

    NON_CACHE = 1  # безналичный
    ADVANCE = 2  # предварительная оплата
    CREDIT = 3  # постоплата
    ANOTHER_FORM = 4  # иная форма оплата


class VatType(enum.Enum):

    WITHOUT = 'none'
    VAT0 = 'vat0'
    VAT10 = 'vat10'
    VAT110 = 'vat110'
    VAT20 = 'vat20'
    VAT120 = 'vat120'


class TransactionStatus(enum.Enum):  # Operation (?) transaction

    CREATED = 'Created'
    PRE_AUTHORIZED_3_DS = 'PreAuthorized3DS'
    PRE_AUTHORIZED_3_DS2 = 'PreAuthorized3DS2'
    PRE_AUTHORIZED_3DS_METHOD = 'PreAuthorized3DSMethod'
    AUTHORIZED = 'Authorized'
    REVERSED = 'Reversed'
    CHARGED = 'Charged'
    REFUNDED = 'Refunded'
    REJECTED = 'Rejected'
    PENDING = 'Pending'
    CREDITED = 'Credited'
