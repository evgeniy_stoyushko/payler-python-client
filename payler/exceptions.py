from .enums import ERROR_MESSAGES

class PaylerError(Exception):

    def __init__(self, error: dict):
        self.code = error.get('code')
        self.message = error.get('message')
        super().__init__(f'{self.code}. {self.message}')


class PaymentError(PaylerError):

    def __init__(self, response):
        self.reason = ''
        super().__init__(response, self.reason)