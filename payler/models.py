from datetime import datetime
from decimal import Decimal
from typing import Optional, List
from .enums import *
from .utils import clean_attrs

__all__ = [
    'Customer',
    'Session',
    'StatusSaveCard',
    'PaymentSession',
    'OperationStatus',
    'RepeatPay',
    'RepeatCredit',
    'OrderItem',
    'VatItem',
    'SupplierInfoItem',
    'SupplierInfo',
    'PaymentItem',
    'OperationAdvancedStatus',
    'Charge',
    'Receipt',
    'ReceiptResponse',
    'SNOType',
    'ReceiptPaymentType',
    'VatType',
    'TransactionStatus',
]


class Model:

    @classmethod
    def from_dict(cls, model_dict):
        raise NotImplementedError

    def __repr__(self):
        state = ', '.join([f'{k}={v}' for k, v in vars(self).items()])
        return f'{self.__class__.__name__} ({state})'


class TransactionModel(Model):

    @classmethod
    def from_dict(cls, model_dict):
        return cls()


class Customer(Model):

    def __init__(self, customer_id: str):
        self.customer_id = customer_id

    @classmethod
    def from_dict(cls, model_dict):
        return cls(customer_id=model_dict.get('customer_id'))


class Session(Model):

    def __init__(self, session_id: str, order_id: str):
        self.session_id = session_id
        self.order_id = order_id

    @classmethod
    def from_dict(cls, model_dict):
        return cls(
            session_id=model_dict.get('session_id'),
            order_id=model_dict.get('order_id'),
        )


class PaymentSession(Model):

    def __init__(self, order_id: str, amount: int, session_id: str):
        self.order_id = order_id
        self.amount = amount
        self.session_id = session_id

    @classmethod
    def from_dict(cls, model_dict):
        return cls(
            order_id=model_dict.get('order_id'),
            amount=model_dict.get('amount'),
            session_id=model_dict.get('session_id'),
        )


class StatusSaveCard(Model):

    def __init__(self, card_id: str, card_status: str, card_number: str,
                 card_holder: str, expired_year: int, expired_month: int,
                 customer_id: str, recurrent_template_id: Optional[str] = None):
        self.card_id = card_id
        self.card_status = CardStatus(card_status)
        self.card_number = card_number
        self.card_holder = card_holder
        self.expired_year = expired_year
        self.expired_month = expired_month
        self.customer_id = customer_id
        self.recurrent_template_id = recurrent_template_id

    @classmethod
    def from_dict(cls, model_dict):
        return cls(
            card_id=model_dict.get('card_id'),
            card_status=model_dict.get('card_status'),
            card_number=model_dict.get('card_number'),
            card_holder=model_dict.get('card_holder'),
            expired_year=model_dict.get('expired_year'),
            expired_month=model_dict.get('expired_month'),
            customer_id=model_dict.get('customer_id'),
            recurrent_template_id=model_dict.get('recurrent_template_id')
        )


class OperationStatus(Model):

    def __init__(self, order_id: str, amount: int, status: str,
                 recurrent_template_id: Optional[str] = None,
                 payment_type: Optional[str] = None):
        self.order_id = order_id
        self.amount = amount
        self.status = TransactionStatus(status)
        self.recurrent_template_id = recurrent_template_id
        self.payment_type = payment_type

    @classmethod
    def from_dict(cls, model_dict):
        return cls(
            order_id=model_dict.get('order_id'),
            amount=model_dict.get('amount'),
            status=model_dict.get('status'),
            recurrent_template_id=model_dict.get('recurrent_template_id'),
            payment_type=model_dict.get('payment_type')
        )


class OperationAdvancedStatus(Model):

    def __init__(self,
                 order_id: str,
                 amount: int,
                 status: str,
                 card_number: str,
                 card_holder: str,
                 expired_year: int,
                 expired_month: int,
                 card_bankname: str,
                 card_paymentsystem: str,
                 card_product: str, dt: str,
                 user_entered_params: dict,
                 card_country_code: Optional[str] = None,
                 recurrent_template_id: Optional[str] = None,
                 from_ip: Optional[str] = None,
                 approval_code: Optional[str] = None,
                 rrn: Optional[str] = None,
                 processing: Optional[str] = None,
                 processing_order_id: Optional[str] = None,
                 currency: Optional[str] = None,
                 card_id: Optional[str] = None,
                 type: Optional[str] = None,
                 # product: Optional[str] = None,
                 # userdata: Optional[str] = None,
                 payment_type: Optional[str] = None,
                 card_status: Optional[str] = None
                 ):

        self.order_id = order_id
        self.amount = amount
        self.status = TransactionStatus(status)
        self.card_number = card_number
        self.card_holder = card_holder
        self.expired_year = expired_year
        self.expired_month = expired_month
        self.card_bankname = card_bankname
        self.card_paymentsystem = card_paymentsystem
        self.card_product = card_product
        self.dt = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
        self.user_entered_params = user_entered_params
        self.card_country_code = card_country_code
        self.recurrent_template_id = recurrent_template_id
        self.from_ip = from_ip
        self.approval_code = approval_code
        self.rrn = rrn
        self.processing = processing
        self.processing_order_id = processing_order_id
        self.currency = Currency(currency)
        self.card_id = card_id
        self.type = type if type is None else PaymentType(type)
        # self.product = product  # ?
        self.payment_type = payment_type  # ?
        # self.user_data = userdata
        self.card_status = card_status if card_status is None \
            else CardStatus(card_status)

    @classmethod
    def from_dict(cls, model_dict):
        return cls(
            order_id=model_dict.get('order_id'),
            amount=model_dict.get('amount'),
            status=model_dict.get('status'),
            card_number=model_dict.get('card_number'),
            card_holder=model_dict.get('card_holder'),
            expired_year=model_dict.get('expired_year'),
            expired_month=model_dict.get('expired_month'),
            card_bankname=model_dict.get('card_bankname'),
            card_paymentsystem=model_dict.get('card_paymentsystem'),
            card_product=model_dict.get('card_product'),
            dt=model_dict.get('dt'),
            user_entered_params=model_dict.get('user_entered_params', {}),
            card_country_code=model_dict.get('card_country_code'),
            # product=model_dict.get('product'),
            recurrent_template_id=model_dict.get('recurrent_template_id'),
            from_ip=model_dict.get('from'),
            approval_code=model_dict.get('approval_code'),
            rrn=model_dict.get('rrn'),
            processing=model_dict.get('processing'),
            processing_order_id=model_dict.get('processing_order_id'),
            currency=model_dict.get('currency'),
            card_id=model_dict.get('card_id'),
            type=model_dict.get('type'),
            payment_type=model_dict.get('payment_type'),
            card_status=model_dict.get('card_status')
        )


class RepeatPay(Model):

    def __init__(self, order_id: str, amount: int, status: str):
        self.order_id = order_id
        self.amount = amount
        self.status = TransactionStatus(status)

    @classmethod
    def from_dict(cls, model_dict):
        return cls(
            order_id=model_dict.get('order_id'),
            amount=model_dict.get('amount'),
            status=model_dict.get('status'),
        )


class RepeatCredit(Model):

    def __init__(self, order_id: str, amount: int, card_number: str,
                 status: str, card_holder: Optional[str] = None):
        self.order_id = order_id
        self.amount = amount
        self.card_number = card_number
        self.status = TransactionStatus(status)
        self.card_holder = card_holder

    @classmethod
    def from_dict(cls, model_dict):
        return cls(
            order_id=model_dict.get('order_id'),
            amount=model_dict.get('amount'),
            card_number=model_dict.get('card_number'),
            status=model_dict.get('status'),
            card_holder=model_dict.get('card_holder'),
        )


class Charge(Model):

    def __init__(self, order_id: str, amount: int, status: str):
        self.order_id = order_id
        self.amount = amount
        self.status = TransactionStatus(status)

    @classmethod
    def from_dict(cls, model_dict):
        return cls(
            order_id=model_dict.get('order_id'),
            amount=model_dict.get('amount'),
            status=model_dict.get('status'),
        )


class VatItem(Model):

    def __init__(self, vat_type: VatType, summ: Optional[Decimal] = None):
        self.vat_type = vat_type.value
        self.summ = summ

    def as_dict(self):
        attrs = {'type': self.vat_type}
        optional_attrs = {'sum': self.summ}
        attrs.update(optional_attrs)
        return attrs


class SupplierInfoItem:

    def __init__(self,
                 phones: Optional[List[str]] = None,
                 name: Optional[str] = None,
                 inn: Optional[str] = None):
        self.phones = phones
        self.name = name
        self.inn = inn

    def as_dict(self):
        optional_attrs = {
            'phones': self.phones,
            'name': self.name,
            'inn': self.inn
        }
        return clean_attrs(optional_attrs)


class OrderItem(Model):

    def __init__(self,
                 name: str,
                 price: float,
                 quantity: float,
                 summ: float,
                 payment_method: PaymentMethodType,
                 payment_object: PaymentObjectType,
                 measurement_unit: Optional[str] = None,
                 nomenclature_code: Optional[str] = None,
                 vat: Optional[VatItem] = None,
                 supplier_info: Optional[SupplierInfoItem] = None):
        self.name = name
        self.price = price
        self.quantity = quantity
        self.summ = summ
        self.measurement_unit = measurement_unit
        self.payment_method = payment_method.value
        self.payment_object = payment_object.value
        self.nomenclature_code = nomenclature_code
        self.vat = vat
        self.supplier_info = supplier_info

    def as_dict(self):
        attrs = {
            'name': self.name,
            'price': f'{self.price}',
            'quantity': f'{self.quantity}',
            'sum': f'{self.summ}',
            'payment_method': self.payment_method,
            'payment_object': self.payment_object,
        }
        optional_attrs = {
            'measure_unit': self.measurement_unit,
            # todo: convert code to hex
            'nomenclature_code': self.nomenclature_code,
            'vat': self.vat.as_dict() if self.vat else None,
            'supplier_info': self.supplier_info.as_dict() if \
                self.supplier_info else None
        }
        attrs.update(clean_attrs(optional_attrs))
        return attrs


class PaymentItem:

    def __init__(self,
                 payment_type: ReceiptPaymentType,
                 summ: float):
        self.payment_type = payment_type.value
        self.summ = summ

    def as_dict(self):
        attrs = {
            'type': self.payment_type,
            'sum': f'{self.summ}'
        }
        return attrs


class SupplierInfo:

    def __init__(self, phones: Optional[List[str]] = None):

        self.phones = phones

    def as_dict(self):
        optional_attrs = {'phones': self.phones}
        return clean_attrs(optional_attrs)


class Receipt:

    def __init__(self,
                 receipt_type: ReceiptType,
                 order_id: str,
                 items: List[OrderItem],
                 sno: SNOType,
                 email: Optional[str] = None,
                 phone: Optional[str] = None,
                 client_name: Optional[str] = None,
                 client_inn: Optional[str] = None,
                 payments: Optional[List[PaymentItem]] = None,
                 vats: Optional[List[VatItem]] = None,
                 supplier_info: Optional[SupplierInfo] = None,
                 agent_type: Optional[AgentType] = None,
                 cashier: Optional[str] = None):

        self.receipt_type = receipt_type
        self.order_id = order_id
        self.items = items
        self.sno = sno
        self.email = email
        self.phone = phone
        self.client_name = client_name
        self.client_inn = client_inn
        self.payments = payments
        self.vats = vats
        self.supplier_info = supplier_info
        self.agent_type = agent_type.value if agent_type else None
        self.cashier = cashier

    def as_dict(self):
        attrs = {
            'order_id': self.order_id,
            'items': [item.as_dict() for item in self.items],
            'type': self.receipt_type.value,
            'sno': self.sno.value
        }
        optionals_attrs = {
            'client_name': self.client_name,
            'client_inn': self.client_inn,
            'payments': [payment.as_dict() for payment in self.payments]
            if self.payments else None,
            'vats': [vat.as_dict() for vat in self.vats] if self.vats else None,
            'email': self.email,
            'phone': self.phone,
            'supplier_info': self.supplier_info.as_dict() if
            self.supplier_info else None,
            'cashier': self.cashier,
            'agent_type': self.agent_type
        }
        attrs.update(clean_attrs(optionals_attrs))
        return attrs


class ReceiptResponse(Model):

    def __init__(self, receipt_id: str):
        self.receipt_id = receipt_id

    @classmethod
    def from_dict(cls, model_dict):
        return cls(
            receipt_id=model_dict.get('receipt_id')
        )