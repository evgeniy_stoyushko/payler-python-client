"""Tests."""
import json
from datetime import datetime
from decimal import Decimal
from unittest import TestCase
from .enums import *
from .models import *


class CustomerModelTest(TestCase):

    def test_reads_customer_from_dict(self):
        model = json.loads('''{
           "customer_id": "Wkl7nBsMhwQICc2I9wXncMeSwnIaucQ5BHyo"
        }''')
        customer = Customer.from_dict(model)
        self.assertEqual(customer.customer_id,
                         'Wkl7nBsMhwQICc2I9wXncMeSwnIaucQ5BHyo')


class SessionModelTest(TestCase):

    def test_reads_session_from_dict(self):
        model = json.loads('''{
            "session_id": "tNx3gy1gDw7Umrhqy0XsLiJMok6F5knFoLgA",
            "order_id": "Wkl7nBsMhwQICc2I9wXncMeSwnIaucQ5BHyo"
        }''')
        session = Session.from_dict(model)
        self.assertEqual(session.session_id,
                         'tNx3gy1gDw7Umrhqy0XsLiJMok6F5knFoLgA')
        self.assertEqual(session.order_id,
                         'Wkl7nBsMhwQICc2I9wXncMeSwnIaucQ5BHyo')


class OperationStatusModelTest(TestCase):

    def test_reads_status_from_dict(self):
        model = json.loads('''{
            "status": "Charged",
            "amount": 30000,
            "recurrent_template_id": "rec-pay-02e20707-eed4-4cb4-9a45-03b5465f8e92",
            "order_id": "a0c799a6-a3df-47cc-b452-63b19d95bb59",
            "payment_type": "Card"
        }''')
        status = OperationStatus.from_dict(model)
        self.assertEqual(status.status, TransactionStatus.CHARGED)
        self.assertEqual(status.amount, 30000)
        self.assertEqual(status.recurrent_template_id,
                         'rec-pay-02e20707-eed4-4cb4-9a45-03b5465f8e92')
        self.assertEqual(status.order_id,
                         'a0c799a6-a3df-47cc-b452-63b19d95bb59')
        self.assertEqual(status.payment_type, 'Card')


class OperationAdvancedStatusModelTest(TestCase):

    def test_reads_advanced_status_from_dict(self):
        model = json.loads('''{
            "card_number": "554781xxxxxx4672",
            "card_holder": "NO NAME",
            "expired_year": 22,
            "expired_month": 3,
            "dt": "2019-06-26 12:59:05",
            "from": "127.0.0.1",
            "approval_code": "598101",
            "rrn": "917791485893",
            "processing": "PSB",
            "currency": "RUB",
            "user_entered_params":
            {
                "user_entered_documentSeria": "1500",
                "user_entered_documentNumbers": "aB"
            },
            "type": "OneStep",
            "card_bankname": "PUBLIC JOINT STOCK COMPANY PROMSVYAZBANK",
            "card_paymentsystem": "MASTERCARD",
            "card_product": "GOLD",
            "card_country_code": "RU",
            "processing_order_id": "1561543145757000",
            "status": "Charged",
            "amount": 500,
            "recurrent_template_id": "rec-pay-da24a6e3-8b9e-4bf2-8fa5-45e5365f5222",
            "payment_type": "Card",
            "order_id": "Dr24ake3-875e-4bf2-8fa5-45e5365f6773"
        }''')
        status = OperationAdvancedStatus.from_dict(model)
        self.assertEqual(status.card_number, '554781xxxxxx4672')
        self.assertEqual(status.card_holder, 'NO NAME')
        self.assertEqual(status.expired_year, 22)
        self.assertEqual(status.expired_month, 3)
        self.assertEqual(status.dt, datetime(year=2019, month=6, day=26,
                                             hour=12, minute=59, second=5))
        self.assertEqual(status.from_ip, '127.0.0.1')
        self.assertEqual(status.approval_code, '598101')
        self.assertEqual(status.rrn, '917791485893')
        self.assertEqual(status.processing, 'PSB')
        self.assertEqual(status.currency, Currency.RUB)
        self.assertEqual(status.type, PaymentType.ONE_STEP)
        self.assertEqual(status.card_bankname,
                         'PUBLIC JOINT STOCK COMPANY PROMSVYAZBANK')
        self.assertEqual(status.card_paymentsystem, 'MASTERCARD')
        self.assertEqual(status.card_product, 'GOLD')
        self.assertEqual(status.card_country_code, 'RU')
        self.assertEqual(status.processing_order_id, '1561543145757000')
        self.assertEqual(status.status, TransactionStatus.CHARGED)
        self.assertEqual(status.amount, 500)
        self.assertEqual(status.recurrent_template_id,
                         'rec-pay-da24a6e3-8b9e-4bf2-8fa5-45e5365f5222')
        self.assertEqual(status.payment_type, 'Card')
        self.assertEqual(status.order_id,
                         'Dr24ake3-875e-4bf2-8fa5-45e5365f6773')
        self.assertEqual(status.user_entered_params,
                         {'user_entered_documentSeria': '1500',
                          'user_entered_documentNumbers': 'aB'})


class PaymentSessionModelTest(TestCase):

    def test_reads_payment_session_from_dict(self):
        model = json.loads('''{
            "amount": 30000,
            "session_id": "FE6nrdcfw4Zy88CRki6sjc1mRxW9xcv7m7CS",
            "order_id": "d1434908-7260-483e-8254-fa43af1b835d"
        }''')
        payment_session = PaymentSession.from_dict(model)
        self.assertEqual(payment_session.amount, 30000)
        self.assertEqual(payment_session.order_id,
                         'd1434908-7260-483e-8254-fa43af1b835d')
        self.assertEqual(payment_session.session_id,
                         'FE6nrdcfw4Zy88CRki6sjc1mRxW9xcv7m7CS')


class ChargeModelTest(TestCase):

    def test_reads_charge_from_dict(self):
        model = json.loads(u'''{ 
            "amount": 3457500,
            "order_id": "sk-d97b9c351818d820fdd3d5d33e6c1edf",
            "status": "Charged"
        }''')
        charge = Charge.from_dict(model)
        self.assertEqual(charge.amount, 3457500)
        self.assertEqual(charge.order_id, 'sk-d97b9c351818d820fdd3d5d33e6c1edf')
        self.assertEqual(charge.status, TransactionStatus.CHARGED)


class RepeatPayModelTest(TestCase):

    def test_reads_repeat_pay_from_dict(self):
        model = json.loads('''{
            "amount": 50000,
            "order_id": "3e31f52f-84bd-4a98-b798-8aafd325a229",
            "status": "Charged"
        }''')
        repeat_pay = RepeatPay.from_dict(model)
        self.assertEqual(repeat_pay.amount, 50000)
        self.assertEqual(repeat_pay.order_id,
                         '3e31f52f-84bd-4a98-b798-8aafd325a229')
        self.assertEqual(repeat_pay.status, TransactionStatus.CHARGED)


class RepeatCreditModelTest(TestCase):

    def test_reads_repeat_credit_from_dict(self):
        model = json.loads('''{
            "amount": 100,
            "card_number": "555555xxxxxx5599",
            "card_holder": "test",
            "order_id": "19",
            "status": "Pending"
        }''')
        repeat_credit = RepeatCredit.from_dict(model)
        self.assertEqual(repeat_credit.amount, 100)
        self.assertEqual(repeat_credit.card_number, '555555xxxxxx5599')
        self.assertEqual(repeat_credit.card_holder, 'test')
        self.assertEqual(repeat_credit.order_id, '19')
        self.assertEqual(repeat_credit.status, TransactionStatus.PENDING)


class ReceiptTest(TestCase):

    def test_generate_dict(self):
        receipt = Receipt(
            receipt_type=ReceiptType.SELL,
            order_id='0e0c0d63-9e29-4d32-a2a3-3c94fb93dace',
            items=[OrderItem(
                name='колбаса Клинский Брауншвейгская с/к в/с',
                price=Decimal(1000),
                quantity=Decimal(0.6),
                summ=Decimal(600),
                measurement_unit='кг',
                payment_method=PaymentMethodType.FULL_PAYMENT,
                payment_object=PaymentObjectType.PAYMENT,
                nomenclature_code='00 00 00 01 00 21 FA 41 00 23 05 41 00 00 00'
                                  ' 00 00 00 00 00 00 00 00 00 00 00 00 12 00 '
                                  'AB 00',
                vat=VatItem(vat_type=VatType.VAT120),
                supplier_info=SupplierInfoItem(phones=['8-111-111-11-11'],
                                               name='ООО «Лютик»',
                                               inn='956839506500')
                )],
            sno=SNOType.OSN,
            client_name='some name',
            client_inn='123456',
            email='test@example.com',
            phone='9268448477',
            agent_type=AgentType.COMMISSION_AGENT,
            cashier='Голева А. С.',
            payments=[PaymentItem(summ=Decimal(500),
                                  payment_type=ReceiptPaymentType.NON_CACHE)],
            vats=[VatItem(vat_type=VatType.VAT20, summ=Decimal(45.76))],
            supplier_info=SupplierInfo(phones=['8-111-111-11-11']),
        )
        receipt_as_dict = receipt.as_dict()
        receipt_as_dict.update({'key': 'd37232ca-cf1d-42f2-a4c3-fb59690e581a'})
        ref_receipt = {
            'key': 'd37232ca-cf1d-42f2-a4c3-fb59690e581a',
            'order_id': '0e0c0d63-9e29-4d32-a2a3-3c94fb93dace',
            'client_name': 'some name',
            'client_inn': '123456',
            'items': [
                {
                    'name': 'колбаса Клинский Брауншвейгская с/к в/с',
                    'price': 1000.00,
                    'quantity': 0.6,
                    'sum': 600.00,
                    'measurement_unit': 'кг',
                    'payment_method': 'full_payment',
                    'payment_object': 'commodity',
                    'nomenclature_code': '00 00 00 01 00 21 FA 41 00 23 05 41 '
                                         '00 00 00 00 00 00 00 00 00 00 00 00 '
                                         '00 00 00 12 00 AB 00',
                    'vat': {'type': 'vat20'},
                    'supplier_info': {
                        'phones': [
                            '8-111-111-11-11'
                        ],
                        'name': 'ООО «Лютик»',
                        'inn': '956839506500'
                    }
                }
            ],
            'payments': [
                {
                    'type': 1,
                    'sum': 500.0
                }
            ],
            'vats': [
                {
                    'type': 'vat20',
                    'sum': 45.76
                }
            ],
            'type': 'sell',
            'email': 'test@example.com',
            'phone': '9268448477',
            'sno': 'osn',
            'supplier_info': {
                'phones': [
                    '8-111-111-11-11'
                ]
            },
            'agent_type': 'commission_agent',
            'cashier': 'Голева А. С.'
        }
        self.assertEqual(receipt_as_dict.get('key'), ref_receipt.get('key'))
        self.assertEqual(receipt_as_dict.get('order_id'),
                         ref_receipt.get('order_id'))
        self.assertEqual(receipt_as_dict.get('client_name'),
                         ref_receipt.get('client_name'))
        self.assertEqual(receipt_as_dict.get('client_inn'),
                         ref_receipt.get('client_inn'))
        self.assertEqual(receipt_as_dict.get('type'), ref_receipt.get('type'))
        self.assertEqual(receipt_as_dict.get('email'), ref_receipt.get('email'))
        self.assertEqual(receipt_as_dict.get('phone'), ref_receipt.get('phone'))
        self.assertEqual(receipt_as_dict.get('sno'), ref_receipt.get('sno'))
        self.assertEqual(receipt_as_dict.get('supplier_info'),
                         ref_receipt.get('supplier_info'))
        self.assertEqual(receipt_as_dict.get('agent_type'),
                         ref_receipt.get('agent_type'))
        self.assertEqual(receipt_as_dict.get('cashier'),
                         ref_receipt.get('cashier'))
        self.assertEqual(receipt_as_dict.get('vats'), ref_receipt.get('vats'))
