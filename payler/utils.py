"""Utils."""


__all__ = ['clean_attrs']


def clean_attrs(func_kwarg: dict) -> dict:
    # for k, v in func_kwarg.items():
    #     if v is not None:
    #         del(func_kwarg[k])
    # return func_kwarg
    return {k: v for k, v in func_kwarg.items() if v is not None}

