from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='payler-python-client',
    version='0.1',
    packages=find_packages(),
    requires=[
        'requests (>=2.22.0)',
    ],
    install_requires = [
        'requests >=2.22.0',
    ],
    long_description=open(join(dirname(__file__), 'README.rst')).read(),
)
